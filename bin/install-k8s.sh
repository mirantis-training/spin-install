#!/bin/sh

#sudo rm -rf .minikube/
#rm -rf .kube

minikube_user="${SUDO_USER:-root}"

curl -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/linux/amd64/kubectl
chmod +x /usr/local/bin/kubectl

curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_1.6.2.deb \
 && sudo dpkg -i minikube_1.6.2.deb

minikube config set vm-driver none
minikube config set dashboard false
minikube config set memory 12000
minikube config set cpus 4

minikube start --bootstrapper kubeadm

chown -R ${minikube_user}:${minikube_user} ~/.kube/ ~/.minikube/

